# Copyright (C) 2021 Andrea G. Monaco
# 
# This file is part of mbdump.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


CFLAGS = -Wall -g


mbdump: main.o
	$(CC) $(CFLAGS) -o mbdump main.o

main.o: main.c
	$(CC) $(CFLAGS) -c main.c



.PHONY: clean

clean:
	rm main.o mbdump
