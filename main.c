/*  Copyright (C) 2021 Andrea G. Monaco
 *
 *  This is mbdump, a program to read and print the multiboot[12] header
 *  of an uncompressed os image.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#define _GNU_SOURCE

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>


enum endianness {BIG_ENDIAN, LITTLE_ENDIAN};


enum endianness detect_system_endianness (void);
void reverse_endianness_in_place (uint8_t *mem, size_t sz);


#define FILE_TO_SYSTEM_ENDIANNESS(mem, size) {\
    if (system_endianness != file_endianness) reverse_endianness_in_place ((uint8_t *)mem, (size_t)size); }


int
main (int argc, char *argv[])
{
  FILE *os;
  size_t rb;

  int _errno;

  enum endianness system_endianness, file_endianness;
  
  /* multiboot2 full header must be in the first 32768 bytes of the file
     (for multiboot1 the limit is 8192 instead) */
  uint8_t start[32768];
  
  uint8_t *magic;
  uint8_t mbmagic_le [] = {0x02, 0xb0, 0xad, 0x1b};
  uint8_t mbmagic_be [] = {0x1b, 0xad, 0xb0, 0x02};
  uint8_t mb2magic_le [] = {0xd6, 0x50, 0x52, 0xe8};
  uint8_t mb2magic_be [] = {0xe8, 0x52, 0x50, 0xd6};
  
  uint32_t *flags, *checksum, *graphic;
  int req_address = 0;
  int req_graph = 0;
  uint32_t *addr;
  
  uint32_t *architecture, *header_length;
  uint16_t *type, *flags2;
  uint32_t *size;

  uint32_t *console_flags;
  uint32_t *mbi_tag_types;

  int mb_found = 0, mb2_found = 0; /* whether we found each one, some kernels might have both */
  
  char tag_names[][40] =
    {
      "",
      "boot command line",
      "boot loader name",
      "name and location of modules",
      "basic memory information",
      "bios boot device",
      "memory map",
      "vbe info",
      "framebuffer info",
      "elf symbols",
      "apm table",
      "efi 32-bit system table pointer",
      "efi 64-bit system table pointer",
      "smbios tables",
      "acpi old rsdp",
      "acpi new rsdp",
      "networking information",
      "efi memory map",
      "efi boot services not terminated",
      "efi 32-bit image handler pointer",
      "efi 64-bit image handler pointer",
      "image load base physical address"
    };

  
  system_endianness = detect_system_endianness ();

  if (argc < 2)
    {
      printf ("usage: %s FILE\n", argv[0]);
      return 1;
    }

  /* the b for binary should be superfluous on unix, but it's in c99 and 
     may be useful for non-unix systems */
  os = fopen (argv[1], "rb");

  if (!os)
    {
      _errno = errno;
      fprintf (stderr, "could not open %s for reading: ", argv[1]);
      errno = _errno;
      perror (NULL);
      return 1;
    }

  rb = fread (start, 1, 32768, os);

  if (!rb)
    {
      _errno = errno;
      fprintf (stderr, "no byte read from %s: ", argv[1]);
      errno = _errno;
      perror (NULL);
      return 1;
    }


  /*
   *   Multiboot 1 
   */
  
  if (((magic = memmem (start, (rb < 8192) ? rb : 8192, mbmagic_le, 4))
       || (magic = memmem (start, (rb < 8192) ? rb : 8192, mbmagic_be, 4))))
    {
      if (!memcmp (magic, mbmagic_le, 4))
	{
	  printf ("multiboot little-endian magic number found at offset %p in %s\n", magic-(uint8_t *)start, argv[1]);
	  file_endianness = LITTLE_ENDIAN;
	}
      else
	{
	  printf ("multiboot big-endian magic number found at offset %p in %s\n", magic-(uint8_t *)start, argv[1]);
	  file_endianness = BIG_ENDIAN;
	}

      mb_found = 1;

      /* multiboot header must be aligned on 32 bits */
      if (((unsigned long)magic - (unsigned long)start) % 4 != 0)
	{
	  printf ("warning: header alignment is not correct, specification requires 4 bytes alignment\n");
	}

      putchar ('\n');
      
      flags = (uint32_t *)magic+1;
      checksum = (uint32_t *)magic+2;

      FILE_TO_SYSTEM_ENDIANNESS (flags, 4);
      FILE_TO_SYSTEM_ENDIANNESS (checksum, 4);
      
      if (*flags & 0x1)
	printf ("os expects boot modules to be aligned on page boundaries (4 KiB)\n");
      else
	printf ("os doesn't expect boot modules to be aligned on page boundaries (4 KiB)\n");
      
      if (*flags & 0x2)
	printf ("bootloader must provide information on available memory via the mem_* fields"
		" in the multiboot information structure\n");
      else
	printf ("bootloader doesn't have to provide information on available memory\n");
      
      if (*flags & 0x4)
	{
	  printf ("information about video mode table must be available to the kernel\n");
	  req_graph = 1;
	}
      else
	printf ("information about video mode table is not requested by the kernel\n");

      if (*flags & 0x10000)
	{
	  printf ("os image requests to be loaded at a specific address provided in this multiboot header\n");
	  req_address = 1;
	}
      else
	printf ("address to be loaded at is not specified by the multiboot header\n");

      if (*(uint32_t *)magic + *flags + *checksum == 0)
	printf ("checksum is valid\n");
      else
	printf ("checksum is not valid\n");


      addr = checksum;
      
      if (req_address)
	{
	  addr++;
	  FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	  printf ("\nthe magic value should be loaded at physical address %x\n", *addr);
	  
	  addr++;
	  FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	  printf ("text segment begins at physical address %x\n", *addr);

	  addr++;
	  FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	  printf ("data segment ends at physical address %x\n", *addr);

	  addr++;
	  FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	  printf ("bss segment ends at physical address %x\n", *addr);

	  addr++;
	  FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	  printf ("entry point of the operating system is at physical address %x\n", *addr);
	}


      graphic = addr;
      
      if (req_graph)
	{
	  printf ("\n");
	  
	  graphic++;
	  FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	  if (*graphic == 0)
	    printf ("os prefers linear graphic mode\n");
	  if (*graphic == 1)
	    printf ("os prefers ega-standard text mode\n");

	  graphic++;
	  FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	  if (*graphic)
	    printf ("preferred width %u\n", *graphic);
	  else
	    printf ("no preferred width\n");
	  
	  graphic++;
	  FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	  if (*graphic)
	    printf ("preferred height %u\n", *graphic);
	  else
	    printf ("no preferred height\n");

	  graphic++;
	  FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	  if (*graphic)
	    printf ("preferred color depth %u\n", *graphic);
	  else
	    printf ("no preferred color depth\n");

	}

    }


  /*
   *   Multiboot 2 
   */

  if (((magic = memmem (start, rb, mb2magic_le, 4)) || (magic = memmem (start, rb, mb2magic_be, 4))))
    {
      if (mb_found)
	printf ("\n\n");
      
      if (!memcmp (magic, mb2magic_le, 4))
	{
	  printf ("multiboot2 little-endian magic number found at offset %p in %s\n", magic-(uint8_t *)start, argv[1]);
	  file_endianness = LITTLE_ENDIAN;
	}
      else
	{
	  printf ("multiboot2 big-endian magic number found at offset %p in %s\n", magic-(uint8_t *)start, argv[1]);
	  file_endianness = BIG_ENDIAN;
	}

      mb2_found = 1;
      
      /* multiboot2 header must be aligned on 64 bits */
      if (((unsigned long)magic - (unsigned long)start) % 8 != 0)
	{
	  printf ("warning: header alignment is not correct, specification requires 8 bytes alignment\n");
	}

      putchar ('\n');
      
      architecture = (uint32_t *)magic + 1;
      header_length = (uint32_t *)magic + 2;
      checksum = (uint32_t *)magic + 3;


      FILE_TO_SYSTEM_ENDIANNESS (architecture, 4);
      if (*architecture == 0)
	printf ("architecture is i386 in protected mode\n");
      if (*architecture == 4)
	printf ("architecture is mips\n");

      FILE_TO_SYSTEM_ENDIANNESS (header_length, 4);
      printf ("header length is %u bytes\n", *header_length);

      FILE_TO_SYSTEM_ENDIANNESS (checksum, 4);
      if (*(uint32_t *)magic + *architecture + *header_length + *checksum == 0)
	printf ("checksum is valid\n");
      else
	printf ("checksum is not valid\n");


      /* jump to the first tag */
      type = (uint16_t *)((uint32_t *)magic + 4);
      FILE_TO_SYSTEM_ENDIANNESS (type, 2);
      flags2 = type+1;
      FILE_TO_SYSTEM_ENDIANNESS (flags2, 2);
      size = (uint32_t *)(flags2 + 1);
      FILE_TO_SYSTEM_ENDIANNESS (size, 4);

      if (*type)
	printf ("\ntags:\n");
      while (*type)
	{
	  if (*flags2 & 0x1)
	    printf ("\noptional:\n");
	  else
	    printf ("\nrequired:\n");
	  
	  if (*type == 1)
	    {
	      printf ("information to the os: ");
	      for (mbi_tag_types = (uint32_t *)type + 2; (unsigned long)mbi_tag_types <
		     (unsigned long)type + *size; mbi_tag_types++)
		{
		  FILE_TO_SYSTEM_ENDIANNESS (mbi_tag_types, 4);
		  printf ("%s; ", tag_names [*mbi_tag_types]);
		}

	      printf ("\n");
	    }

	  else if (*type == 2)
	    {
	      addr = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("the magic value should be loaded at physical address %x\n", *addr);
	  
	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("text segment begins at physical address %x\n", *addr);

	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("data segment ends at physical address %x\n", *addr);

	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("bss segment ends at physical address %x\n", *addr);
	    }

	  else if (*type == 3)
	    {
	      addr = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("entry point of the operating system is at physical address %x\n", *addr);
	    }
	  
	  else if (*type == 4)
	    {
	      console_flags = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (console_flags, 4);
	      if (*console_flags & 0x1)
		printf ("os requires at least one of the following supported consoles and"
			" information about it must be passed in mbi\n");
	      if (*console_flags & 0x2)
		printf ("os supports ega text\n");

	      if (*size != 12)
		printf ("warning: this tag should have size 12, has %u instead\n", *size);
	    }

	  else if (*type == 5)
	    {
	      graphic = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	      if (*graphic == 0)
		printf ("os has no preferred width\n");
	      else 
		printf ("preferred width %u\n", *graphic);
	  
	      graphic++;
	      FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	      if (*graphic == 0)
		printf ("os has no preferred height\n");
	      else 
		printf ("preferred height %u\n", *graphic);
      
	      graphic++;
	      FILE_TO_SYSTEM_ENDIANNESS (graphic, 4);
	      if (*graphic == 0)
		printf ("os has no preferred color depth\n");
	      else 
		printf ("preferred color depth %u\n", *graphic);

	      if (*size != 20)
		printf ("warning: this tag should have size 20, has %u instead\n", *size);
	    }

	  else if (*type == 6)
	    {
	      printf ("os expects boot modules to be aligned on page boundaries (4 KiB)\n");

	      if (*size != 8)
		printf ("warning: this tag should have size 8, has %u instead\n", *size);
	    }

	  else if (*type == 7)
	    {
	      printf ("payload can be started without terminating boot services\n");

	      if (*size != 8)
		printf ("warning: this tag should have size 8, has %u instead\n", *size);
	    }

	  else if (*type == 8)
	    {
	      addr = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("entry point of efi i386 compatible code is at physical address %p\n", addr);
	    }

	  else if (*type == 9)
	    {
	      addr = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("entry point of efi amd64 compatible code is at physical address %p\n", addr);
	    }

	  else if (*type == 10)
	    {
	      printf ("image is relocatable\n");

	      addr = size+1;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("lowest possible physical address is %p\n", addr);

	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("highest possible physical address is %p\n", addr);

	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      printf ("image alignment is %p\n", addr);

	      addr++;
	      FILE_TO_SYSTEM_ENDIANNESS (addr, 4);
	      if (*addr == 0)
		printf ("no placement suggestion\n");
	      if (*addr == 1)
		printf ("preferably load at lowest address\n");
	      if (*addr == 2)
		printf ("preferably load at highest address\n");

	      if (*size != 24)
		printf ("warning: this tag should have size 24, has %u instead\n", *size);
	    }

	  else
	    {
	      printf ("warning: unknown tag type %d\n", *type);
	    }

	  
	  /* jump to the next tag, which must be aligned on 64 bits */
	  type = (uint16_t *)((uint8_t *)type + *size);
	  if ((unsigned long)type % 8)
	    type = (uint16_t *)((unsigned long)type + (8 - (unsigned long)type % 8));
	  FILE_TO_SYSTEM_ENDIANNESS (type, 2);

	  flags2 = type+1;
	  FILE_TO_SYSTEM_ENDIANNESS (flags2, 2);
	  size = (uint32_t *)(flags2 + 1);
	  FILE_TO_SYSTEM_ENDIANNESS (size, 4);

	  if ((unsigned long)type + *size > (unsigned long)magic + *header_length
	      || (unsigned long)type + *size > (unsigned long)magic + 32768)
	    {
	      printf ("\nwarning: next tag goes beyond end of header\n");
	      break;
	    }
	  if (!*type)
	    {
	      if (*size != 8)
		{
		  printf ("\nwarning: closing tag should have size 8, has %u instead\n", *size);
		  break;
		}

	      printf ("\nclosing tag found\n");
	      break;
	    }
	}

    }

  if (!mb_found && !mb2_found)
    {
      printf ("no multiboot 1 or 2 magic number found in %s\n", argv[1]);
      return 1;
    }
  
  return 0;  
}


enum endianness
detect_system_endianness (void)
{
  uint32_t a = 1;
  uint32_t *b = &a;
  uint8_t *c = (uint8_t *)b; 
  if (*c)
    return LITTLE_ENDIAN;
  return BIG_ENDIAN;
}


void
reverse_endianness_in_place (uint8_t *mem, size_t sz)
{
  int i;
  uint8_t tmp;

  for (i = 0; i < sz/2; i++)
    {
      tmp = mem[i];
      mem[i] = mem[sz-i-1];
      mem[sz-i-1] = tmp;
    }
}
